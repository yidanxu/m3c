"""M3C 2018 Homework 1
Name: Yidan Xu
CID: 01205667
"""

import numpy as np
import matplotlib.pyplot as plt


def plot_S(S):
    """Simple function to create plot from input S matrix
    """
    ind_s0 = np.where(S==0) #M locations
    ind_s1 = np.where(S==1) #C locations
    plt.plot(ind_s0[1],ind_s0[0],'rs')
    plt.hold(True)
    plt.plot(ind_s1[1],ind_s1[0],'bs')
    plt.hold(False)
    plt.show()
    plt.pause(0.05)
    return None

def simulate1(N,Nt,b,e):
    """Simulate C vs. M competition on N x N grid over
    Nt generations. b and e are model parameters
    to be used in fitness calculations
    Output: S: Status of each gridpoint at tend of somulation, 0=M, 1=C
    fc: fraction of villages which are C at all Nt+1 times
    Do not modify input or return statement without instructor's permission.
    """
    #Set initial condition
    S  = np.ones((N,N),dtype=int) #Status of each gridpoint: 0=M, 1=C
    j = int((N-1)/2)
    S[j-1:j+2,j-1:j+2] = 0

    fc = np.zeros(Nt+1) #Fraction of points which are C
    fc[0] = S.sum()/(N*N)
    return S,fc

def fitness_sc(S,N,mat_neighbours,b,e):
    """Generate the fitness score for each village with 8 moved frames
    """
    #Initialise for the zip loop
    z1 = np.asarray([0, 1, 2]*3)
    z2 = np.repeat(np.array((0, 1, 2)), 3)

    # to create a frame to move around the central matrix
    # initialise W1 to be within a frame of zeros
    W1 = np.zeros((N+2, N+2))
    W1[1:N+1, 1:N+1] = S

    #Initialise W2 being the matrix with fitness score for positions being C
    # bounded by a frame
    W2 = np.zeros((N+2, N+2))

    for i, j in zip(z1, z2):
        W_ite = np.zeros((N+2, N+2))
        W_ite[i:(N+i), j:(N+j)] = S
        W2 += W_ite

    #removing the original S bounded with frame
    W2 += - W1
    #remove the frame for W2
    W2 = W2[1:N+1, 1:N+1]

    #A temporary matrix to store the number of C being neighbours of a M
    W3 = W2 * (S==0)
    mat_neighbours_c = mat_neighbours * (S==0)
    E = (mat_neighbours_c - W3) * e

    #The total score for M, C is
    Wm = (E + W3 * b)/mat_neighbours
    Wc = (W2 * (S==1))/mat_neighbours
    #The combined score is thus
    Ws = Wm + Wc

    return Ws, Wc


def simulate2ite(S,N,b,e,mat_neighbours,debug=False):
    """Iteration step for a stochastic process over Nt years
    """
    #randomly generated probability from normal distribution
    Q = np.random.uniform(size=(N,N))

    #Initialise for the zip loop
    z1 = np.asarray([0, 1, 2]*3)
    z2 = np.repeat(np.array((0, 1, 2)), 3)

    #calculate the fiteness score for each villages
    Ws, Wc = fitness_sc(S,N,mat_neighbours,b,e)

    #Calculate for the probability of becoming C the next year
    #Approach similar as above
    W_tot = np.zeros((N+2, N+2))
    W_tot_c = np.zeros((N+2, N+2))
    for i, j in zip(z1, z2):
        W_ite = np.zeros((N+2, N+2))
        W_ite[i:(N+i), j:(N+j)] = Ws
        W_tot += W_ite

        #find also the total score for C in a community
        W_ite_c = np.zeros((N+2, N+2))
        W_ite_c[i:(N+i), j:(N+j)] = Wc
        W_tot_c += W_ite_c

    #Remove the frame of W_tot and W_tot_c first
    # just in case a zero in denominator
    Pc = W_tot_c[1:N+1, 1:N+1]/W_tot[1:N+1, 1:N+1]

    #If the probability of becoming C is greater than the random matrix
    # the village is converted to C next year

    #update S, year and fc array
    S_update = (Pc > Q) * 1
    fci = S_update.sum()/(N * N)

    if debug:
        plot_S(S_update)

    return S_update, fci


#Do the simulation for Nt years
def simulate2(N,Nt,b,e,debug=False):
    """Simulate the stochastic progression of the change of status, C, M
    of the villages contained
    """
    #Initialise the status matrix
    S_, fc = simulate1(N,Nt,b,e)

    #Generate a matrix to store the number of neighbours
    mat_neighbours = np.ones((N, N))*8
    mat_neighbours[:, ::N-1] = 5
    mat_neighbours[::N-1, :] = 5
    mat_neighbours[::N-1, ::N-1] = 3

    year = 0
    #While not yet reaching Nt years
    while year < Nt:
        S_, fci = simulate2ite(S_,N,b,e,mat_neighbours,debug=debug)

        year += 1
        fc[year] = fci
        if debug:
            print("year is", year)

    return S_, fc


def analyze(j,m,N,Nt,b,e,debug=False):
    """m is the number of simulations for each b
    N is the size of the square status matrix S
    Nt is the number of years a simulation is run for
    b is the score for each of M a neighbour is M
    e is the score for each of M a neighbour is C
    """
    for i in range(1,m+1):
        S, fc = mf.simulate2(N,Nt,b,e,debug=False)

        plt.plot(fc)
        plt.hold(True)

    plt.hold(False)

    plt.title("Yidan Xu: simulate2 b = {}".format(b))
    plt.xlabel('Nt/years')
    plt.ylabel('fc')

    plt.savefig('hw1{}.png'.format(j), dpi = 1000)

if __name__ == '__main__':
    #The code here should call analyze and generate the
    #figures that you are submitting with your code
    m = 20
    N = 21
    Nt = 200
    e = 0.01
    b_array = np.array((1.03, 1.08, 1.3, 1.45))

    for j in range(len(b_array)):
        analyze(j+1, 20, 21, 200, b_array[j], 0.01)
