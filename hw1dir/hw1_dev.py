"""M3C 2018 Homework 1
Name: Yidan Xu
CID: 01205667
"""

import numpy as np
import matplotlib.pyplot as plt

def simulate1(N,Nt,b,e):
    """Simulate C vs. M competition on N x N grid over
    Nt generations. b and e are model parameters
    to be used in fitness calculations
    Output: S: Status of each gridpoint at tend of somulation, 0=M, 1=C
    fc: fraction of villages which are C at all Nt+1 times
    Do not modify input or return statement without instructor's permission.
    """
    #Set initial condition
    S  = np.ones((N,N),dtype=int) #Status of each gridpoint: 0=M, 1=C
    j = int((N-1)/2)
    S[j-1:j+2,j-1:j+2] = 0

    fc = np.zeros(Nt+1) #Fraction of points which are C
    fc[0] = S.sum()/(N*N)
    return S,fc

def plot_S(S):
    """Simple function to create plot from input S matrix
    """
    ind_s0 = np.where(S==0) #M locations
    ind_s1 = np.where(S==1) #C locations
    plt.plot(ind_s0[1],ind_s0[0],'rs')
    plt.hold(True)
    plt.plot(ind_s1[1],ind_s1[0],'bs')
    plt.hold(False)
    plt.show()
    plt.pause(0.05)
    return None

"""To construct one iteration for the process first
"""
def simulate2ite(W,N,b,e,mat_neighbours,debug=False):
    """Simulation code for Part 2
    initialisation:
    matrix W the input status matrix with zeroes around
    matrix R for points of the villaegs
    matrix P the probability of turning to Crespectively
    matrix Q a randomly generated probability to compare with P
    output: final updated W and portion of C among N*N villages
    """

    #Initialise matrices
    R = np.zeros((N, N))
    P = np.zeros((N, N))
    # Q = np.zeros((N, N))
    Z = np.zeros((N+2, N+2))
    # randomly generated probability from uniform distribution
    Q = np.random.uniform(size=(N,N))

    #Add total points for each C and W village respectively
    for i in range(1,N+1):
        for j in range(1,N+1):
            if W[i, j] == 1:
                fit_score_c = ((np.sum(W[i-1:i+2, j-1:j+2]) - 1)
                                /mat_neighbours[i-1, j-1])
                R[i-1, j-1] = fit_score_c
            else:
                num_adjacentc = np.sum(W[i-1:i+2, j-1:j+2])
                num_neighbour = mat_neighbours[i-1, j-1]
                fit_score_m = (e * (num_neighbour - num_adjacentc)
                                + b * num_adjacentc)/num_neighbour
                R[i-1, j-1] = fit_score_m
    Z[1:N+1, 1:N+1] = R
    #print(R)
    #Calculate for the probability of being C, W respectively
    for i in range(1,N+1):
        for j in range(1,N+1):
            mat_community = W[i-1:i+2, j-1:j+2]
            mat_r_comm = Z[i-1:i+2, j-1:j+2]
            # print(mat_community)
            # print(mat_r_comm)

            tot_fit_c = np.sum(mat_community * mat_r_comm)
            # tot_fit_m = np.sum((np.ones((3, 3)) - mat_community) * mat_r_comm)
            tot_fit = np.sum(mat_r_comm)

            prob_be_c = tot_fit_c/tot_fit
            # prob_be_m = tot_fit_m/tot_fit
            # print(prob_be_m)
            P[i-1, j-1] = prob_be_c
            # Q[i-1, j-1] = prob_be_m

    #If the probability of becoming C is greater than the random matrix
    #The village is converted to C next year
    W_update = (P > Q) * 1
    fc = W_update.sum()/(N * N)

    if debug:
        plot_S(W_update)

    return W_update, fc


"""find the situation in the end of Nt years
"""
def simulate2(N,Nt,b,e,debug=False):

    #Initialise status matrix and the vector for fraction of C
    S, fc = simulate1(N,Nt,b,e)
    #Add extra 0s around the matrix
    # to avoid considering for boundaries
    W = np.zeros((N+2, N+2), dtype=int)
    # ind_W0 = np.where(S == 0)
    # ind_W1 = np.where(S == 1)
    W[1:N+1, 1:N+1] = S

    #Generate a matrix to store the number of neighbours
    mat_neighbours = np.ones((N, N))*8
    mat_neighbours[:, ::N-1] = 5
    mat_neighbours[::N-1, :] = 5
    mat_neighbours[::N-1, ::N-1] = 3

    year = 0
    #While not yet reaching Nt years
    while year < Nt:
        W_temp, fc[year] = simulate2ite(W,N,b,e,mat_neighbours,debug=debug)

        # ind_W0 = np.where(W_temp == 0)
        # ind_W1 = np.where(W_temp == 1)
        if debug:
            print("For year %s", year)

        W[1:N+1, 1:N+1] = W_temp
        year += 1

    return W, fc


def analyze(j,m,N,Nt,b,e,debug=False):
    """m is the number of simulations for each b
    N is the size of the square status matrix S
    Nt is the number of years a simulation is run for
    b is the score for each of M a neighbour is M
    e is the score for each of M a neighbour is C
    """
    for i in range(1,m+1):
        S, fc = mf.simulate2(N,Nt,b,e,debug=False)

        plt.plot(fc)
        plt.hold(True)

    plt.hold(False)

    plt.title("Yidan Xu: simulate2 b = {}".format(b))
    plt.xlabel('Nt/years')
    plt.ylabel('fc')

    plt.savefig('hw1{}.png'.format(j), dpi = 1000)


if __name__ == '__main__':
    #The code here should call analyze and generate the
    #figures that you are submitting with your code
    m = 20
    N = 21
    Nt = 200
    e = 0.01
    b_array = np.array((1.03, 1.08, 1.3, 1.45))

    for j in range(len(b_array)):
        analyze(j+1,m,N,Nt,b,e,debug=False)
